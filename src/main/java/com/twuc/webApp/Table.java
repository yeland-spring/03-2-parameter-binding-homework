package com.twuc.webApp;

public class Table {
    private int start;
    private int end;
    private String operator;

    public Table(int start, int end, String operator) {
        this.start = start;
        this.end = end;
        this.operator = operator;
    }

    public String getTable() {
        StringBuilder result = new StringBuilder();
        for (int left = start; left <= end; left++) {
            for (int right = start; right <= left; right++) {
                result.append(formatFormula(left, right));
            }
            result.append("\n");
        }
        return result.toString();
    }

    private String formatFormula(int left, int right) {
        String formula = new Formula(left, right, operator).getFormula();
        String maxFormula =new Formula(end, right, operator).getFormula();
        if (formula.length() < maxFormula.length()) {
            return formula + " ";
        }
        return formula;
    }
}
