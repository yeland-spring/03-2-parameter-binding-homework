package com.twuc.webApp;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class CheckController {
    @PostMapping("/api/check")
    String getEqualCorrect(@RequestBody @Valid Formula formula) {
        return formula.isCorrect();
    }
}
