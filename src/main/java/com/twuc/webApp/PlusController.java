package com.twuc.webApp;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PlusController {
    @GetMapping(value = "/api/tables/plus")
    public ResponseEntity getTable(@RequestParam(defaultValue = "1") int start, @RequestParam(defaultValue = "9") int end) {
        Table table = new Table(start, end, "+");
        if (start > end) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("");
        }
        return ResponseEntity.ok().body(table.getTable());
    }
}
