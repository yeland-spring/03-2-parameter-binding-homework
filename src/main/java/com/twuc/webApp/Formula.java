package com.twuc.webApp;

import javax.validation.constraints.NotNull;

public class Formula {
    @NotNull
    private Integer operandLeft;
    @NotNull
    private Integer operandRight;
    @NotNull
    private String operation;
    @NotNull
    private Integer expectedResult;
    private String checkType;
    private Integer result;

    public Formula(Integer operandLeft, Integer operandRight, String operation, Integer expectedResult, String checkType) {
        this.operandLeft = operandLeft;
        this.operandRight = operandRight;
        this.operation = operation;
        this.expectedResult = expectedResult;
        this.checkType = checkType;
    }

    public Formula(@NotNull Integer operandLeft, @NotNull Integer operandRight, @NotNull String operation, @NotNull Integer expectedResult) {
        this.operandLeft = operandLeft;
        this.operandRight = operandRight;
        this.operation = operation;
        this.expectedResult = expectedResult;
        this.checkType = "=";
    }

    public Formula(Integer operandLeft, Integer operandRight, String operation) {
        this.operandLeft = operandLeft;
        this.operandRight = operandRight;
        this.operation = operation;
    }

    public Formula() {
    }

    public Integer getOperandLeft() {
        return operandLeft;
    }

    public Integer getOperandRight() {
        return operandRight;
    }

    public String getOperation() {
        return operation;
    }

    public Integer getExpectedResult() {
        return expectedResult;
    }

    public String getCheckType() {
        return checkType;
    }

    public String getFormula() {
        result = (this.operation.equals("+")) ? operandLeft + this.operandRight : this.operandLeft * this.operandRight;
        String formula = String.format("%d%s%d=%d", operandLeft, this.operation, this.operandRight, result);
        return formula + " ";
    }

    public String isCorrect() {
        getFormula();
        switch (checkType) {
            case ">":
                return String.valueOf(expectedResult > result);
            case "<":
                return String.valueOf(expectedResult < result);
            default:
                return String.valueOf(expectedResult.equals(result));
        }
    }
}
