package com.twuc.webApp;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@AutoConfigureMockMvc
class CheckControllerIntegrationTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    void should_return_200_with_correct_when_request() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/check")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(new ObjectMapper().writeValueAsString(new Formula(1, 2, "+", 3))))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("true"));
    }

    @Test
    void should_return_200_false_correct_when_request() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/check")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(new ObjectMapper().writeValueAsString(new Formula(1, 2, "+", 5))))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("false"));
    }

    @Test
    void should_return_400_when_param_is_null() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/check")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(new ObjectMapper().writeValueAsString(new Formula(3, 2, "+" ))))
                .andExpect(MockMvcResultMatchers.status().is(400));
    }

    @Test
    void should_return_200_when_add_check_type() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/check")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(new ObjectMapper().writeValueAsString(new Formula(1, 2, "+", 5, ">"))))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("true"));
    }
}